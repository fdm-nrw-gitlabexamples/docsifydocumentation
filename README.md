# Docsify Documentation Template

This is a simple template to demonstrate gitlab pages using [docsify](https://docsify.js.org/).

Docsify renders markdown files for display in the browser.

This text is read from the README.md of the project.

You can create other files like [HelloWorld.md](./HelloWorld.md) that can be loaded by docsify.

## Quickstart

1. Create a fork of this repository or copy all the files to a new repository
1. Add a license of your choosing e.g. see https://choosealicense.com/non-software/
    * Update `LICENSE` file
    * Update `config.js` with license information
1. Update other settings in `config.js`
1. Add more `*.md` files for your content
1. Add navigation links to sidebar (`sidebar.md`) and top navigation (`nav.md`)

After pushing to the `main` branch, your page will be automatically updated.

## How This Works in Detail

This repository uses the GitLab Pages feature to serve a documentation page using docsify.

## About Docsify

Docsify is a small client side renderer for markdown files. It comes as a set of files (`.html`, `.js`, `.css`) that can be served via HTTP. These files reside in the `.public` Folder of this repository.

When the docsify page is loaded it also loads the `README.md` file from the same folder and displays the rendered contents to the user. Just like the home page of a gitlab repositroy does, but without the GitLab user interface around it. Instead docsify adds navigation bars on the top and in a sidebar. You can modify the contents of the navigation by editing the files `nav.md` for the top navigation and `sidebar.md` for the sidebar. Docsify will also automatically add the headings of the current markdown file to the sidebar for navigation on the page.

For more details on the features of docsify consult the offical homepage: https://docsify.js.org/.

## About GitLab Pages

GitLab Pages is a feature of GitLab that allows to publish static files as web pages served by GitLab itself - no additional hosting is needed. 

To enable GitLab Pages you need a GitLab CI job that is called `pages`. Everytime this job is run, it will pick up the contents of the folder `public` within the jobs artifacts and serve those as static files on the webserver. You can create the contents of the `public` folder in the CI job but it can also be files that reside in the `public` folder of your repository. 

## Putting Things Together

To serve our `README.md` from the GitLab Pages server, we need to copy that file to the `public` folder during the CI job. Additionally we want to serve the boilerplate files (`.html`, `.js`, `.css`) from docsify. These files reside in a folder `.public` (note the `.`). The GitLab CI script of the `pages` job copies all files from the repository and the boilerplate files into the `public` folder so that they can be picked up by GitLab Pages.
let footer = '';

// CC-BY-SA-4.0 Footer
footer = 
`
<hr/>
<footer>
  <a href="https://creativecommons.org/licenses/by-sa/4.0" target="_blank">
    <img src="https://mirrors.creativecommons.org/presskit/buttons/80x15/svg/by-sa.svg" width=80 alt="CC-BY-SA-4.0" />
  </a>
  <span>M. Politze (2021): Beispielseite mit Docsify</span>
</footer>
`;

// ----- CC0 License START -----
// Footer, remove this block to use CC-BY-SA-4 from above
footer = 
`
<hr/>
<footer>
  <a href="https://creativecommons.org/publicdomain/zero/1.0/" target="_blank">
    <img src="https://mirrors.creativecommons.org/presskit/buttons/80x15/svg/cc-zero.svg" width=80 alt="CC0-1.0" />
  </a>
  <span>M. Politze (2021): Beispielseite mit Docsify</span>
</footer>
`;
// ----- CC0 License END -----

// More Docsify configuration see:
// https://docsify.js.org/#/configuration
 window.$docsify = {
      name: '',
      repo: '',
      loadNavbar: 'nav.md',
      loadSidebar: 'sidebar.md',
      maxLevel: 4,
      subMaxLevel: 2,
      auto2top: true,
      logo: '/img/logo.png',
      name: 'Beispielseite mit Docsify',
      plugins: [
        function (hook) {
          hook.afterEach(function (html) {
            return html + footer
          })
        }
      ]
    }